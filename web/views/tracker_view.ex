defmodule Gaja.TrackerView do
  use Gaja.Web, :view
  
  def render("index.json", %{trackers: trackers}) do
    %{data: render_many(trackers, Gaja.TrackerView, "tracker.json")}
  end
  
  def render("show.json", %{tracker: tracker}) do
    %{data: render_one(tracker, Gaja.TrackerView, "tracker.json")}
  end
  
  def render("tracker.json", %{tracker: tracker}) do
    %{
      uuid: tracker.id,
      name: tracker.name,
      inserted_at: tracker.inserted_at,
      updated_at: tracker.updated_at
    }
  end
end
