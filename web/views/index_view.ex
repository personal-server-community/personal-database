defmodule Gaja.IndexView do
  use Gaja.Web, :view
  
  def render("index.json", info_map) do
    %{
      name: info_map[:name],
      version: info_map[:version]
    }
  end
end
