defmodule Gaja.AuthView do
  use Gaja.Web, :view

  def render("logged_in.json", _assigns) do
    %{success: "true", message: "Anonymously logged in."}
  end
end
