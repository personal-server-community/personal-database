defmodule Gaja.SessionView do
  use Gaja.Web, :view
  
  def render("authenticated.json", _) do
    %{
      authenticated: true
    }
  end
 
  def render("info.json", %{info: token}) do
    %{
      access_token: token
    }
  end
end
