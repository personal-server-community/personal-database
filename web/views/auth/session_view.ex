defmodule Gaja.Web.SessionView do
  use Gaja.Web, :view

  def render("info.json", %{info: token}) do
    %{access_token: token}
  end
end
