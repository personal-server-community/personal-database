defmodule Gaja.TrackerEntryView do
  use Gaja.Web, :view
  
  def render("index.json", %{tracker_entries: tracker_entries}) do
    %{data: render_many(tracker_entries, Gaja.TrackerEntryView, "tracker_entry.json")}
  end
  
  def render("show.json", %{tracker_entry: tracker_entry}) do
    %{data: render_one(tracker_entry, Gaja.TrackerEntryView, "tracker_entry.json")}
  end
  
  def render("tracker_entry.json", %{tracker_entry: tracker_entry}) do
    %{
      uuid: tracker_entry.id,
      date: tracker_entry.date,
      value: tracker_entry.value,
      value_string: tracker_entry.value_string
    }
  end
end
