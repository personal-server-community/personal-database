defmodule Gaja.UserView do
  use Gaja.Web, :view
  
  def render("index.json", %{users: users}) do
    %{data: render_many(users, Gaja.UserView, "user.json")}
  end
  
  def render("show.json", %{user: user}) do
    %{data: render_one(user, Gaja.UserView, "user.json")}
  end
  
  def render("user.json", %{user: user}) do
    %{
      uuid: user.id,
      name: user.name,
      email: user.email,
      inserted_at: user.inserted_at,
      updated_at: user.updated_at
    }
  end

end
