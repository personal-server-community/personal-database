defmodule Gaja.IndexController do
  use Gaja.Web, :controller

  def index(conn, _params) do
    render(conn, "index.json", %{
      name: "Gaja",
      version: "0.1.0"
    })
  end
end
