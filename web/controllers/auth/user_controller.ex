defmodule Gaja.UserController do
  use Gaja.Web, :controller

  import Gaja.Web.Authorize

  alias Phauxth.Log
  alias Gaja.Accounts

  action_fallback Gaja.Web.FallbackController

  plug :user_check when action in [:index, :show]
  plug :id_check when action in [:update, :delete]

  def index(conn, _) do
    users = Accounts.list_users()
    render(conn, "index.json", users: users)
  end

  def create(conn, %{"user" => user_params}) do
    with {:ok, user} <- Accounts.create_user(user_params) do
      Log.info(%Log{user: user.id, message: "user created"})
      conn
      |> put_status(:created)
      |> put_resp_header(
        "location", 
        Gaja.Router.Helpers.user_path(conn, :show, user)
      )
      |> render("show.json", user: user)
    end
  end

  def show(%Plug.Conn{assigns: %{current_user: user}} = conn, %{"id" => id}) do
    user = if id == to_string(user.id), do: user, else: Accounts.get_user(id)
    render(conn, "show.json", user: user)
  end

  def update(%Plug.Conn{assigns: %{current_user: user}} = conn, %{"user" => user_params}) do
    changeset = Accounts.update_user(user, user_params)
    case changeset do
     {:ok, user} -> render(conn, "show.json", user: user)
     {:error, _} -> changeset
    end
  end

  def delete(%Plug.Conn{assigns: %{current_user: user}} = conn, _) do
    {:ok, _user} = Accounts.delete_user(user)
    send_resp(conn, :no_content, "")
  end
end
