defmodule Gaja.SessionController do
  use Gaja.Web, :controller
  import Gaja.Web.Authorize

  alias Gaja.Sessions
  alias Phauxth.Login
  alias Gaja.Web.Auth.Token

  plug :guest_check when action in [:create]
  plug :user_check when action in [:index]

  def index(conn, %{}) do
    render(conn, "authenticated.json")
  end

  def create(conn, %{"session" => params}) do
    case Login.verify(params) do
      {:ok, user} ->
        {:ok, %{id: session_id}} = Sessions.create_session(%{user_id: user.id})
        token = Token.sign(%{session_id: session_id})
        render(conn, "info.json", %{info: token})
      {:error, _message} ->
        error(conn, :unauthorized, 401)
    end
  end
end
