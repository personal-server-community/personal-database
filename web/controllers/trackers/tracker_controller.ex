defmodule Gaja.TrackerController do
  use Gaja.Web, :controller

  import Gaja.Web.Authorize

  alias Gaja.{Tracker, Trackers}

  action_fallback Gaja.Web.FallbackController

  plug :user_check when action in [:index, :show, :create, :update, :delete]

  def index(%Plug.Conn{assigns: %{current_user: user}} = conn, _params) do
    trackers = Trackers.list_trackers(user)
    render(conn, "index.json", trackers: trackers)
  end

  def show(%Plug.Conn{assigns: %{current_user: user}} = conn, %{"id" => uuid}) do
    with tracker = %Tracker{} <- Trackers.get_tracker(user, uuid) do
      render(conn, "show.json", tracker: tracker)
    end
  end

  def create(%Plug.Conn{assigns: %{current_user: user}} = conn, params) do
    with {:ok, tracker} <- Trackers.create_tracker(user, params) do
      conn
      |> put_status(201)
      |> render("show.json", tracker: tracker)
    end
  end

  def delete(%Plug.Conn{assigns: %{current_user: user}} = conn, %{"id" => uuid}) do
    with tracker = %Tracker{} <- Trackers.get_tracker(user, uuid) do
      Trackers.delete_tracker(tracker)
      conn
      |> put_status(204)
      |> send_resp(:no_content, "")
    end
  end

  def update(
    %Plug.Conn{assigns: %{current_user: user}} = conn,
    %{"id" => uuid, "tracker" => tracker_params}
  ) do
    with tracker = %Tracker{} <- Trackers.get_tracker(user, uuid) do
      changeset = Trackers.update_tracker(tracker, tracker_params)
      case changeset do
       {:ok, tracker} -> render(conn, "show.json", tracker: tracker)
       {:error, _} -> changeset
      end
    end
  end
end
