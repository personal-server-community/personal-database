defmodule Gaja.TrackerEntryController do
  use Gaja.Web, :controller
  import Gaja.Web.Authorize
  alias Gaja.{TrackerEntry, TrackerEntries, Trackers}

  action_fallback Gaja.Web.FallbackController

  plug :user_check when action in [
    :index, :show, :create, :update, :delete
  ]

  def index(
    %Plug.Conn{assigns: %{current_user: user}} = conn, 
    %{"tracker_id" => tracker_id, "start_date" => start_date, "end_date" => end_date}
  ) do
    tracker = Trackers.get_tracker(user, tracker_id)
    tracker_entries = TrackerEntries.list_tracker_entries(tracker, start_date, end_date)
    render(conn, "index.json", tracker_entries: tracker_entries)
  end

  def index(
    %Plug.Conn{assigns: %{current_user: user}} = conn, 
    %{"tracker_id" => tracker_id}
  ) do
    tracker = Trackers.get_tracker(user, tracker_id)
    tracker_entries = TrackerEntries.list_tracker_entries(tracker)
    render(conn, "index.json", tracker_entries: tracker_entries)
  end

  def show(
    %Plug.Conn{assigns: %{current_user: user}} = conn, 
    %{"tracker_id" => tracker_id, "id" => uuid}
  ) do
    tracker = Trackers.get_tracker(user, tracker_id)
    with tracker_entry = %TrackerEntry{} <- TrackerEntries.get_tracker_entry(tracker, uuid) do
      render(conn, "show.json", tracker_entry: tracker_entry)
    end
  end

  def create(%Plug.Conn{assigns: %{current_user: user}} = conn, params) do
    tracker = Trackers.get_tracker(user, params["tracker_id"])
    with {:ok, tracker_entry} <- TrackerEntries.create_tracker_entry(tracker, params) do
      Gaja.Endpoint.broadcast(
        "trackers:room", 
        "tracker-entry:new", 
        %{
          id: tracker_entry.id,
          tracker_id: tracker_entry.tracker_id,
          date: tracker_entry.date,
          value: tracker_entry.value
        }
      )
      conn
      |> put_status(201)
      |> render("show.json", tracker_entry: tracker_entry)
    end
  end

  def update(%Plug.Conn{assigns: %{current_user: user}} = conn, params) do
    tracker = Trackers.get_tracker(user, params["tracker_id"])
    with tracker_entry = %TrackerEntry{} <- TrackerEntries.get_tracker_entry(tracker, params["id"]) do
      changeset = TrackerEntries.update_tracker_entry(tracker_entry, params["tracker_entry"])
      Gaja.Endpoint.broadcast(
        "trackers:room", 
        "tracker-entry:update", 
        %{
          id: tracker_entry.id,
          tracker_id: tracker_entry.tracker_id,
          date: tracker_entry.date,
          value: tracker_entry.value
        }
      )
      case changeset do
       {:ok, tracker_entry} -> render(conn, "show.json", tracker_entry: tracker_entry)
       {:error, _} -> changeset
      end
    end
  end

  def delete(%Plug.Conn{assigns: %{current_user: user}} = conn, params) do
    tracker = Trackers.get_tracker(user, params["tracker_id"])
    with tracker_entry = %TrackerEntry{} <- TrackerEntries.get_tracker_entry(tracker, params["id"]) do
      Gaja.Endpoint.broadcast(
        "trackers:room", 
        "tracker-entry:delete", 
        %{
          id: tracker_entry.id,
          tracker_id: tracker_entry.tracker_id,
          date: tracker_entry.date,
          value: tracker_entry.value
        }
      )
      TrackerEntries.delete_tracker_entry(tracker_entry) 
      conn
      |> put_status(204)
      |> send_resp(:no_content, "")
    end
  end
end
