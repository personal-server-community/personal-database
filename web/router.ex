defmodule Gaja.Router do
  use Gaja.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :authenticated do
    plug :fetch_session
    plug :put_secure_browser_headers
    plug Phauxth.AuthenticateToken
  end

  scope "/", Gaja do
    pipe_through :api
    get "/", IndexController, :index
  end

  scope "/api", Gaja do
    pipe_through [
      :api,
      :authenticated
    ]

    get "/authenticated", SessionController, :index
    post "/sessions", SessionController, :create
    resources "/users", UserController, except: [:new, :edit]
    resources "/trackers", TrackerController, except: [:new, :edit] do
      resources "/entries", TrackerEntryController, except: [:new, :edit]
    end
  end
end
