defmodule Gaja.Web.Auth.Token do
  @moduledoc """
  Custom token implementation using Phauxth.Token behaviour and Phoenix Token.
  """
  @behaviour Phauxth.Token

  alias Phoenix.Token
  alias Gaja.Endpoint
  require Logger

  @max_age 14_400
  @token_salt "B35o23Hu"

  @impl true
  def sign(data, opts \\ []) do
    Logger.debug("sign ok")
    Token.sign(Endpoint, @token_salt, data, opts)
  end

  @impl true
  def verify(token, opts \\ []) do
    Logger.debug("verify token ok")
    Token.verify(Endpoint, @token_salt, token, opts ++ [max_age: @max_age])
  end
end
