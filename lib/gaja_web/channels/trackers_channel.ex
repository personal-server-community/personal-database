defmodule Gaja.Web.TrackersChannel do
  @moduledoc false
  use Gaja.Web, :channel

  def join("trackers:room", payload, socket) do
    if authorized?(payload) do
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end
end
