defmodule Gaja.Trackers do
  @moduledoc false
  import Ecto
  import Ecto.Query, warn: false
  alias Gaja.{Tracker, Repo}
  alias Gaja.Accounts.{User}

  def list_trackers(user) do
    Repo.all(Ecto.assoc(user, :trackers))
  end

  def create_tracker(user, attrs) do
    user
    |> build_assoc(:trackers)
    |> Tracker.create_changeset(attrs)
    |> Repo.insert()
  end

  def get_tracker(%User{id: user_id}, id) do
    Tracker
    |> where([t], t.id == ^id and t.user_id == ^user_id)
    |> Repo.one()
  end

  def update_tracker(%Tracker{} = tracker, attrs) do
    tracker
    |> Tracker.update_changeset(attrs)
    |> Repo.update()
  end

  def delete_tracker(%Tracker{} = tracker) do
    Repo.delete(tracker)
  end

  def get_by(attrs) do
    Repo.get_by(Tracker, attrs)
  end
end
