defmodule Gaja.TrackerEntry do
  @moduledoc false

  use Ecto.Schema
  use Gaja.Web, :model
  import Ecto.Changeset

  schema "tracker_entries" do
    field(:date, :naive_datetime)
    field(:value, :float)
    field(:value_string, :string)
    belongs_to(:tracker, Gaja.Tracker)

    timestamps()
  end

  @doc false
  def changeset(tracker, attrs) do
    tracker
    |> cast(attrs, [:date, :value, :value_string])
    |> validate_required([:date])
  end
end
