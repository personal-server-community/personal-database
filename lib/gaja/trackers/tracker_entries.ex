defmodule Gaja.TrackerEntries do
  @moduledoc false

  import Ecto
  import Ecto.Query, warn: false
  alias Gaja.{Tracker, TrackerEntry, Repo}

  def create_tracker_entry(tracker, attrs) do
    tracker
    |> build_assoc(:tracker_entries)
    |> TrackerEntry.changeset(attrs)
    |> Repo.insert()
  end

  def list_tracker_entries(tracker) do
    Repo.all(Ecto.assoc(tracker, :tracker_entries))
  end

  def list_tracker_entries(%Tracker{id: tracker_id}, start_date, end_date) do
    TrackerEntry
    |> where([t], t.tracker_id == ^tracker_id)
    |> where([t], t.date >= ^start_date and t.date <= ^end_date)
    |> Repo.all()
  end

  def get_tracker_entry(%Tracker{id: tracker_id}, id) do
    TrackerEntry
    |> where([t], t.id == ^id and t.tracker_id == ^tracker_id)
    |> Repo.one()
  end

  def get_by(attrs) do
    Repo.get_by(TrackerEntry, attrs)
  end

  def update_tracker_entry(%TrackerEntry{} = tracker_entry, attrs) do
    tracker_entry
    |> TrackerEntry.changeset(attrs)
    |> Repo.update()
  end

  def delete_tracker_entry(%TrackerEntry{} = tracker_entry) do
    Repo.delete(tracker_entry)
  end
end
