defmodule Gaja.Tracker do
  @moduledoc false

  use Ecto.Schema
  use Gaja.Web, :model

  import Ecto.Changeset

  schema "trackers" do
    field(:name, :string)
    has_many(:tracker_entries, Gaja.TrackerEntry)
    belongs_to(:user, Gaja.Accounts.User)

    timestamps()
  end

  @doc false
  def create_changeset(%__MODULE__{} = tracker, attrs) do
    changeset(tracker, attrs)
  end

  @doc false
  def update_changeset(%__MODULE__{} = tracker, attrs) do
    changeset(tracker, attrs)
  end

  @doc false
  def changeset(%__MODULE__{} = tracker, attrs) do
    tracker
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
