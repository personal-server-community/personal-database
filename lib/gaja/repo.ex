defmodule Gaja.Repo do
  use Ecto.Repo,
    otp_app: :gaja,
    adapter: Ecto.Adapters.Postgres

  def init(_, opts) do
    {:ok, build_opts(opts)}
  end

  defp build_opts(opts) do
    system_opts = [
      database: System.get_env("DATABASE_NAME"),
      hostname: System.get_env("DATABASE_HOST"),
      password: System.get_env("DATABASE_PASS"),
      username: System.get_env("DATABASE_USER"),
      port: System.get_env("DATABASE_PORT")
    ]

    system_opts
    |> remove_empty_opts()
    |> merge_opts(opts)
  end

  defp merge_opts(system_opts, opts) do
    Keyword.merge(opts, system_opts)
  end

  defp remove_empty_opts(system_opts) do
    Enum.reject(system_opts, fn {_k, value} -> is_nil(value) end)
  end
end
