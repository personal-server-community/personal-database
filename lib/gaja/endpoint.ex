defmodule Gaja.Endpoint do
  use Phoenix.Endpoint, otp_app: :gaja

  socket "/socket", Gaja.Web.UserSocket,
    # or list of options
    websocket: true,
    longpoll: false

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    plug Phoenix.CodeReloader
  end

  plug Plug.RequestId
  plug Plug.Logger

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Jason

  plug Plug.MethodOverride
  plug Plug.Head

  # The session will be stored in the cookie and signed,
  # this means its contents can be read but not tampered with.
  # Set :encryption_salt if you would also like to encrypt it.
  plug Plug.Session,
    store: :cookie,
    key: "_gaja_key",
    signing_salt: "yRAUYsE8"

  plug Gaja.Router

  def init(_type, config) do
    port = System.get_env("PORT") || 4000
    host = System.get_env("HOST") || "localhost"
    config = Keyword.put(config, :http, [:inet6, port: port])
    config = Keyword.put(config, :url, host: host, port: port)
    {:ok, config}
  end
end
