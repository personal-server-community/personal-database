ARG ALPINE_VERSION=3.8

FROM elixir:1.7.2-alpine AS builder

ARG APP_NAME
ARG APP_VSN
ARG MIX_ENV
ARG PHOENIX_SUBDIR=.

ENV APP_NAME=${APP_NAME} \
    APP_VSN=${APP_VSN} \
    MIX_ENV=${MIX_ENV}

# By convention, /opt is typically used for applications
WORKDIR /opt/gaja

# This step installs all the build tools we'll need
RUN apk update && \
  apk upgrade --no-cache && \
  apk add --no-cache \
    git \
    build-base && \
  mix local.rebar --force && \
  mix local.hex --force

RUN git clone --single-branch --depth 1 https://gitlab.com/personal-server-community/personal-database.git /opt/gaja

RUN mix do deps.get, deps.compile, compile

RUN \
  mkdir -p /opt/built && \
  mix distillery.release --env=prod --name=gaja --verbose && \
  cp _build/${MIX_ENV}/rel/${APP_NAME}/releases/${APP_VSN}/${APP_NAME}.tar.gz /opt/built && \
  cd /opt/built && \
  tar -xzf ${APP_NAME}.tar.gz && \
  rm ${APP_NAME}.tar.gz

FROM elixir:1.7.2-alpine

ARG APP_NAME=gaja

RUN apk update && \
    apk add --no-cache \
      bash \
      openssl-dev

ENV REPLACE_OS_VARS=true \
    APP_NAME=${APP_NAME}

RUN apk add postgresql-client

WORKDIR /opt/gaja

COPY --from=builder /opt/built .
CMD /opt/gaja/bin/gaja create && \
    /opt/gaja/bin/gaja migrate && \
    /opt/gaja/bin/gaja foreground
