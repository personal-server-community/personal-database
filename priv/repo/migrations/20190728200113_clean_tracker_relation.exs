defmodule Gaja.Repo.Migrations.CleanTrackerRelation do
  use Ecto.Migration

  def change do
    alter table(:trackers) do
      add(:user_id, references(:users, type: :binary_id), null: false)
    end
  end
end
