defmodule Gaja.Repo.Migrations.AddTrackersTable do
  use Ecto.Migration

  def change do
    create table(:trackers, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:name, :string)
      timestamps()
    end

    create(unique_index(:trackers, [:name]))
  end
end
