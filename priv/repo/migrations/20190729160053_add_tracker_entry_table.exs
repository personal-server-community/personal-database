defmodule Gaja.Repo.Migrations.AddTrackerEntryTable do
  use Ecto.Migration

  def change do
    create table(:tracker_entries, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:value, :float)
      add(:value_string, :string)
      add(:date, :utc_datetime, default: fragment("now()"))
      add(:tracker_id, references(:trackers, type: :binary_id), null: false)
      timestamps()
    end

    create(index(:tracker_entries, [:date]))
  end
end
