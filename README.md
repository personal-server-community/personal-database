# Gaja

Simple API to store all your personal data in a place of your own.

## Status

[![pipeline
status](https://gitlab.com/personal-server-community/personal-database/badges/master/pipeline.svg)](https://gitlab.com/personal-server-community/personal-database/commits/master)

[![coverage
report](https://gitlab.com/personal-server-community/personal-database/badges/master/coverage.svg)](https://gitlab.com/personal-server-community/personal-database/commits/master)

## Build

To build the source into a binary file dedicated to production environment, run:

```
make build
```

## Docker container

If you need the application in a container, run:

```
make build_docker
```

A docker image named `gaja` will be available in your docker local registry.

The recipe to deploy it via docker compose is the following:

```
version: '3'

services:
   database:
    restart: always
    image: postgres:latest
    environment:
      - POSTGRES_PASSWORD=password
    volumes:
      - dbdata:/var/lib/postgresql/data

  app:
    restart: always
    depends_on:
      - database
    image: gaja 
    links:
      - database
    environment:
      - DATABASE_USER='postgres'
      - DATABASE_PASS='password'
      - DATABASE_HOST='database'
      - DATABASE_NAME='gaja'

volumes:
  dbdata:
    external: false
```

## Development environment

To set up a development environment, clone this repository then do the
following:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Start Phoenix endpoint with `mix phoenix.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

You can run tests with:

```
mix test --trace --seed 0
```
