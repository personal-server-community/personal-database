defmodule Gaja.AuthCase do
  @moduledoc false

  use Phoenix.ConnTest
  alias Gaja.Accounts
  alias Gaja.Web.Auth.Token

  def add_user(email) do
    add_user(email, "Robin")
  end

  def add_user(email, name) do
    user = %{
      name: name,
      email: email,
      password: "reallyHard2gue$$"
    }

    {:ok, user} = Accounts.create_user(user)
    user
  end

  def add_token_conn(conn, user) do
    user_token = Token.sign(%{"user_id" => user.id})

    conn
    |> put_req_header("accept", "application/json")
    |> put_req_header("authorization", user_token)
  end
end
