defmodule Gaja.Web.SessionControllerTest do
  use Gaja.ConnCase

  import Gaja.AuthCase

  @create_attrs %{
    email: "robin@example.com",
    password: "reallyHard2gue$$"
  }
  @invalid_attrs %{
    email: "robin@example.com",
    password: "cannotGue$$it"
  }

  setup %{conn: conn} do
    user = add_user("robin@example.com", "Robin")
    {:ok, %{conn: conn, user: user}}
  end

  describe "create session" do
    test "login succeeds", %{conn: conn} do
      conn =
        post(
          conn,
          Routes.session_path(conn, :create),
          session: @create_attrs
        )

      assert json_response(conn, 200)["access_token"]
    end

    test "login fails when log in twice", %{conn: conn, user: user} do
      conn = conn |> add_token_conn(user)

      conn =
        post(
          conn,
          Routes.session_path(conn, :create),
          session: @create_attrs
        )

      message = json_response(conn, 401)["message"]
      assert message
      assert message =~ "Anonymously logged in"
    end

    test "login fails for invalid password", %{conn: conn} do
      conn =
        post(
          conn,
          Routes.session_path(conn, :create),
          session: @invalid_attrs
        )

      assert json_response(conn, 401)["errors"]["detail"] =~ "Not authenticated"
    end
  end
end
