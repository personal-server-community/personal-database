defmodule Gaja.Web.TrackerControllerTest do
  use Gaja.ConnCase

  import Gaja.AuthCase
  alias Gaja.Trackers

  setup %{conn: conn} = config do
    email = config[:login] || "default@example.com"
    user = add_user(email)

    attrs = %{
      user: user,
      name: "First tracker"
    }

    if config[:login] do
      {:ok, tracker} = Trackers.create_tracker(user, attrs)
      conn = conn |> add_token_conn(user)
      {:ok, %{conn: conn, tracker: tracker, user: user}}
    else
      {:ok, tracker} = Trackers.create_tracker(user, attrs)
      {:ok, %{conn: conn, tracker: tracker, user: user}}
    end
  end

  describe "index |" do
    @tag login: "bill@example.com"
    test "lists all trackers", %{conn: conn} do
      conn = get(conn, Routes.tracker_path(conn, :index))
      assert json_response(conn, 200)
    end

    test "401 for unauthorized user", %{conn: conn} do
      conn = get(conn, Routes.tracker_path(conn, :index))
      assert json_response(conn, 401)
    end
  end

  describe "show |" do
    @tag login: "bill@example.com"
    test "show tracker info", %{conn: conn, tracker: tracker} do
      conn = get(conn, Routes.tracker_path(conn, :show, tracker))
      assert json_response(conn, 200)["data"]["uuid"] == tracker.id
    end

    test "401 for unauthorized user", %{conn: conn, tracker: tracker} do
      conn = get(conn, Routes.tracker_path(conn, :show, tracker))
      assert json_response(conn, 401)
    end
  end

  describe "create |" do
    @tag login: "bill@example.com"
    test "data is valid", %{conn: conn, user: user} do
      attrs = %{
        user: user,
        name: "New tracker"
      }

      conn =
        post(
          conn,
          Routes.tracker_path(conn, :create),
          attrs
        )

      assert json_response(conn, 201)["data"]["uuid"]
      assert Trackers.get_by(name: "New tracker")
      assert length(Trackers.list_trackers(user)) == 2
    end

    @tag login: "bill@example.com"
    test "data is invalid", %{conn: conn, user: user} do
      attrs = %{
        user: user,
        name: nil
      }

      conn = post(conn, Routes.tracker_path(conn, :create), tracker: attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end

    test "401 for unauthorized user", %{conn: conn, user: user} do
      attrs = %{
        user: user,
        name: "New tracker"
      }

      conn = post(conn, Routes.tracker_path(conn, :create), tracker: attrs)
      assert json_response(conn, 401)
    end
  end

  describe "update |" do
    @tag login: "reg@example.com"
    test "data is valid", %{conn: conn, user: user, tracker: tracker} do
      attrs = %{
        name: "New name"
      }

      conn = put(conn, Routes.tracker_path(conn, :update, tracker.id), tracker: attrs)
      assert json_response(conn, 200)
      updated_tracker = Trackers.get_tracker(user, tracker.id)
      assert updated_tracker.name == "New name"
    end

    @tag login: "reg@example.com"
    test "data is invalid", %{conn: conn, tracker: tracker} do
      invalid_attrs = %{
        name: nil
      }

      conn = put(conn, Routes.tracker_path(conn, :update, tracker.id), tracker: invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete |" do
    @tag login: "reg@example.com"
    test "success", %{conn: conn, user: user, tracker: tracker} do
      conn = delete(conn, Routes.tracker_path(conn, :delete, tracker.id))
      assert response(conn, 204)
      refute Trackers.get_tracker(user, tracker.id)
    end

    test "401 for unauthorized user", %{conn: conn, tracker: tracker} do
      conn = delete(conn, Routes.tracker_path(conn, :delete, tracker))
      assert json_response(conn, 401)
    end
  end
end
