defmodule Gaja.Web.TrackerEntryControllerTest do
  use Gaja.ConnCase

  import Gaja.AuthCase
  alias Gaja.{Trackers, TrackerEntries}

  setup %{conn: conn} = config do
    email = config[:login] || "default@example.com"
    user = add_user(email)

    tracker_attrs = %{
      name: "My tracker"
    }

    attrs = %{
      value: 8,
      date: "2019-08-01T00:00:00"
    }

    {:ok, tracker} = Trackers.create_tracker(user, tracker_attrs)
    {:ok, tracker_entry} = TrackerEntries.create_tracker_entry(tracker, attrs)

    if config[:login] do
      conn = conn |> add_token_conn(user)
      {:ok, %{conn: conn, user: user, tracker: tracker, tracker_entry: tracker_entry}}
    else
      {:ok, %{conn: conn, user: user, tracker: tracker, tracker_entry: tracker_entry}}
    end
  end

  describe "index |" do
    @tag login: "bill@example.com"
    test "lists all tracker entries", %{conn: conn, tracker: tracker} do
      conn = get(conn, Routes.tracker_tracker_entry_path(conn, :index, tracker))
      assert json_response(conn, 200)
    end

    @tag login: "bill@example.com"
    test "between given dates", %{conn: conn, tracker: tracker} do
      attrs = %{value: 2, date: "2019-06-01T00:00:00"}
      TrackerEntries.create_tracker_entry(tracker, attrs)
      attrs = %{value: 5, date: "2019-07-01T00:00:00"}

      {:ok, tracker_entry} =
        TrackerEntries.create_tracker_entry(
          tracker,
          attrs
        )

      response =
        conn
        |> get(
          Routes.tracker_tracker_entry_path(conn, :index, tracker),
          start_date: "2019-06-15T00:00:00",
          end_date: "2019-07-15T00:00:00"
        )
        |> json_response(200)

      expected = %{
        "data" => [
          %{
            "date" => "2019-07-01T00:00:00",
            "uuid" => tracker_entry.id,
            "value" => 5.0,
            "value_string" => nil
          }
        ]
      }

      assert response == expected
    end

    test "401 for unauthorized user", %{conn: conn, tracker: tracker} do
      conn = get(conn, Routes.tracker_tracker_entry_path(conn, :index, tracker))
      assert json_response(conn, 401)
    end
  end

  describe "create |" do
    @tag login: "bill@example.com"
    test "data is valid", %{conn: conn, tracker: tracker} do
      attrs = %{
        value: 8,
        date: "2019-08-01T00:00:00"
      }

      conn =
        post(
          conn,
          Routes.tracker_tracker_entry_path(conn, :create, tracker),
          attrs
        )

      assert json_response(conn, 201)["data"]["uuid"]
      # assert TrackerEntries.get_by date: "New tracker"
      assert length(TrackerEntries.list_tracker_entries(tracker)) == 2
    end

    @tag login: "bill@example.com"
    test "data is invalid", %{conn: conn, tracker: tracker} do
      attrs = %{
        value: nil,
        date: "2019-08-01T00:00:00"
      }

      conn = post(conn, Routes.tracker_tracker_entry_path(conn, :create, tracker), tracker: attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end

    test "401 for unauthorized user", %{conn: conn, tracker: tracker} do
      attrs = %{
        value: 8,
        date: "2019-08-01T00:00:00"
      }

      conn = post(conn, Routes.tracker_tracker_entry_path(conn, :create, tracker), tracker: attrs)
      assert json_response(conn, 401)
    end
  end

  describe "show |" do
    @tag login: "bill@example.com"
    test "show tracker entry info", %{conn: conn, tracker: tracker, tracker_entry: tracker_entry} do
      conn = get(conn, Routes.tracker_tracker_entry_path(conn, :show, tracker, tracker_entry))
      assert json_response(conn, 200)["data"]["uuid"] == tracker_entry.id
    end

    test "401 for unauthorized user", %{
      conn: conn,
      tracker: tracker,
      tracker_entry: tracker_entry
    } do
      conn = get(conn, Routes.tracker_tracker_entry_path(conn, :show, tracker, tracker_entry))
      assert json_response(conn, 401)
    end
  end

  describe "update |" do
    @tag login: "reg@example.com"
    test "data is valid", %{conn: conn, tracker: tracker, tracker_entry: tracker_entry} do
      attrs = %{
        value: 6.0
      }

      conn =
        put(conn, Routes.tracker_tracker_entry_path(conn, :update, tracker, tracker_entry),
          tracker_entry: attrs
        )

      assert json_response(conn, 200)
      updated_tracker_entry = TrackerEntries.get_tracker_entry(tracker, tracker_entry.id)
      assert updated_tracker_entry.value == 6.0
    end

    @tag login: "reg@example.com"
    test "data is invalid", %{conn: conn, tracker: tracker, tracker_entry: tracker_entry} do
      invalid_attrs = %{
        value: "test"
      }

      conn =
        put(conn, Routes.tracker_tracker_entry_path(conn, :update, tracker, tracker_entry),
          tracker_entry: invalid_attrs
        )

      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete |" do
    @tag login: "reg@example.com"
    test(
      "success",
      %{conn: conn, tracker: tracker, tracker_entry: tracker_entry}
    ) do
      conn =
        delete(
          conn,
          Routes.tracker_tracker_entry_path(
            conn,
            :delete,
            tracker,
            tracker_entry
          )
        )

      assert response(conn, 204)
      refute TrackerEntries.get_tracker_entry(tracker, tracker_entry.id)
    end

    test "401 for unauthorized user", %{
      conn: conn,
      tracker: tracker,
      tracker_entry: tracker_entry
    } do
      conn =
        delete(conn, Routes.tracker_tracker_entry_path(conn, :delete, tracker, tracker_entry))

      assert json_response(conn, 401)
    end
  end
end
