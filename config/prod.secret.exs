use Mix.Config

# In this file, we keep production configuration that
# you'll likely want to automate and keep away from
# your version control system.
#
# You should document the content of this
# file or create a script for recreating it, since it's
# kept out of version control and might be hard to recover
# or recreate for your teammates (or yourself later on).
config :gaja, Gaja.Endpoint, secret_key_base: System.get_env("SECRET_KEY_BASE")

# Configure your database
config :gaja, Gaja.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "mysecretpassword",
  database: "gaja",
  hostname: "localhost",
  port: "5432",
  pool_size: 15
