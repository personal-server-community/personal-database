# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :gaja,
  ecto_repos: [Gaja.Repo]

# Configures the endpoint
config :gaja, Gaja.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "B9ney6bs6p3KIWVSjKJgalmKjXFHmTj+D9+i5L7XEyHmxPapwZw2FBKttY/X3IgM",
  render_errors: [view: Gaja.ErrorView, accepts: ~w(json)],
  pubsub: [name: Gaja.PubSub, adapter: Phoenix.PubSub.PG2]

# Phauxth authentication configuration
config :phauxth,
  user_context: Gaja.Accounts,
  crypto_module: Argon2,
  token_module: Gaja.Web.Auth.Token,
  token_salt: "IYCkNK5rZhjGd7mg"

# Configures Elixir's 
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Configure phoenix generators
config :phoenix, :generators, binary_id: true

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
