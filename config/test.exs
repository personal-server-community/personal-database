use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :gaja, Gaja.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :gaja, Gaja.Repo,
  username: System.get_env("DATABASE_USER", "postgres"),
  password: System.get_env("DATABASE_PASS", "mysecretpassword"),
  database: System.get_env("DATABASE_NAME", "gaja_test"),
  hostname: System.get_env("DATABASE_HOST", "localhost"),
  pool: Ecto.Adapters.SQL.Sandbox

config :argon2_elixir, t_cost: 1, m_cost: 8
